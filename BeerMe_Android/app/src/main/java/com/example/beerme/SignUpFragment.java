package com.example.beerme;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;



public class SignUpFragment extends Fragment {

    // Listener declared
    private Listener mListener;

    public SignUpFragment() {
    }

    public static SignUpFragment newInstance() {

        Bundle args = new Bundle();

        SignUpFragment fragment = new SignUpFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof SignUpFragment.Listener) {
            mListener = (SignUpFragment.Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void submitNewUser(String password, String email);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null && getContext() != null) {
            final EditText passwordET = getView().findViewById(R.id.passwordET);
            final EditText emailET = getView().findViewById(R.id.emailET);
            final CheckBox termsCHK = getView().findViewById(R.id.termsChkBx);
            ImageButton createAcctBtn = getView().findViewById(R.id.signUpImgBtn);



            createAcctBtn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    final String password = passwordET.getText().toString().trim();
                    final String email = emailET.getText().toString().trim();
                    final boolean checked = termsCHK.isChecked();

                    if (!password.equals("") && !email.equals("")) {

                        if (checked) {
                            mListener.submitNewUser(email, password);
                        } else {
                            Toast.makeText(getContext(), "You must accept the terms and conditions " +
                                    "if you wish to use BeerMe", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    } else {
                        Toast.makeText(getContext(), "Please do not leave anything blank", Toast.LENGTH_SHORT)
                                .show();
                    }
                }
            });
        }
    }
}
