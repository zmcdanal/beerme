package com.example.beerme;

import java.io.Serializable;

public class BeersTried implements Serializable {

    // Declarations
    private String id;
    private String name;
    private int rating;
    private double cost;
    private double abv;
    private String lastDrank;
    private int amountDrank;
    private String notes;
    private String breweryFrom;

    public BeersTried() {

    }

    // Constructor
    public BeersTried(String id, String name, int rating, double cost, double abv, String lastDrank, int amountDrank, String notes, String breweryFrom) {
        this.id = id;
        this.name = name;
        this.rating = rating;
        this.cost = cost;
        this.abv = abv;
        this.lastDrank = lastDrank;
        this.amountDrank = amountDrank;
        this.notes = notes;
        this.breweryFrom = breweryFrom;
    }

    // Getters
    public String getId() {
        return id;
    }

    public int getAmountDrank() {
        return amountDrank;
    }

    public int getRating() {
        return rating;
    }

    public double getCost() {
        return cost;
    }

    public double getAbv() {
        return abv;
    }

    public String getName() {
        return name;
    }

    public String getStringRating() {
        return String.valueOf(rating);
    }

    public String getStringCost() {
        return String.valueOf(cost);
    }

    public String getStringABV() {
        return String.valueOf(abv);
    }

    public String getLastDrank() {
        return lastDrank;
    }

    public String getStringAmountDrank() {
        return String.valueOf(amountDrank);
    }

    public String getNotes() {
        return notes;
    }

    public String getBreweryFrom() {
        return breweryFrom;
    }

    // Setters
    public void setLastDrank(String lastDrank) {
        this.lastDrank = lastDrank;
    }

    public void setBreweryFrom(String breweryFrom) {
        this.breweryFrom = breweryFrom;
    }

    public void setAmountDrank(int amountDrank) {
        this.amountDrank = amountDrank;
    }
}
