package com.example.beerme;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

public class SelectedBreweryFragment extends ListFragment {

    // Various Variables
    private static final String TAG = "SelectedBreweryFragment";
    private Brewery brewery;
    private Listener mListener;

    public SelectedBreweryFragment() {
    }

    public static SelectedBreweryFragment newInstance(Brewery brewery) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, brewery);
        SelectedBreweryFragment fragment = new SelectedBreweryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof SelectedBreweryFragment.Listener) {
            mListener = (SelectedBreweryFragment.Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void addBeer();
        void toBeerDetails(BeersTried beersTried);
        void backToMap(Brewery brewery);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_selected_brewery, container, false);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null && getView() != null) {
            brewery = (Brewery) getArguments().getSerializable(TAG);

            if (brewery != null) {

                // findViewByIds
                TextView title = getView().findViewById(R.id.breweryNameTV);
                TextView timesVisited = getView().findViewById(R.id.timesVisitedTV);
                TextView website = getView().findViewById(R.id.websiteTV);
                TextView phone = getView().findViewById(R.id.phoneTV);
                TextView lastVisited = getView().findViewById(R.id.lastVisitedTV);

                String finalizedNumFirstStep = brewery.getPhone().replaceAll("[()\\-\\s]", "").trim();
                String finalizedNumSecondStep = finalizedNumFirstStep.replaceFirst("(\\d{3})(\\d{3})(\\d+)", "($1)$2-$3");

                // Setting text for page
                title.setText(brewery.getName());
                lastVisited.setText(brewery.getLastVisited());
                timesVisited.setText(brewery.getStringTimesVisited());
                website.setText(brewery.getUrl());
                phone.setText(finalizedNumSecondStep);

                Linkify.addLinks(phone, Patterns.PHONE,"tel:",Linkify.sPhoneNumberMatchFilter,Linkify.sPhoneNumberTransformFilter);
                phone.setMovementMethod(LinkMovementMethod.getInstance());

                // List Adapter is set
                setListAdapter(new BeersTriedListAdapter(brewery.getBeers(), getContext()));

                /* Star Button Setup */
                checkRating(brewery.getRating());

                /* List Setup */
                final ImageButton addBeer = getView().findViewById(R.id.addBeerImgBtn);
                addBeer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.addBeer();
                    }
                });

                /* Back to Map button setup */
                ImageButton backToMap = getView().findViewById(R.id.backToMapImgBtn);
                backToMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.backToMap(brewery);
                    }
                });

            }

        }
    }

    // Segues to Beer Profile with selected beer
    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(TAG, "onListItemClick: " + brewery.getBeers().get(position).getName());
        mListener.toBeerDetails(brewery.getBeers().get(position));
    }

    // Checks Rating and adds appropriate star
    private void checkRating(double rating) {

        if (getView() != null) {
            ImageView star1 = getView().findViewById(R.id.star1);
            ImageView star2 = getView().findViewById(R.id.star2);
            ImageView star3 = getView().findViewById(R.id.star3);
            ImageView star4 = getView().findViewById(R.id.star4);
            ImageView star5 = getView().findViewById(R.id.star5);

            // Ratings handled
            if (rating >= 1) {
                star1.setImageResource(R.drawable.star);
            } else {
                star1.setImageResource(R.drawable.emptystar);
            }

            if (rating >= 2) {
                star2.setImageResource(R.drawable.star);
            } else {
                star2.setImageResource(R.drawable.emptystar);
            }

            if (rating >= 3) {
                star3.setImageResource(R.drawable.star);
            } else {
                star3.setImageResource(R.drawable.emptystar);
            }

            if (rating >= 4) {
                star4.setImageResource(R.drawable.star);
            } else {
                star4.setImageResource(R.drawable.emptystar);
            }

            if (rating == 5) {
                star5.setImageResource(R.drawable.star);
            } else {
                star5.setImageResource(R.drawable.emptystar);
            }
        }
    }

}
