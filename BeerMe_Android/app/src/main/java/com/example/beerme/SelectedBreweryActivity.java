package com.example.beerme;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SelectedBreweryActivity extends AppCompatActivity implements SelectedBreweryFragment.Listener {

    private static final String TAG = "SelectedBreweryActivity";
    public static final int TO_ADD_BEER = 1;
    public static final int TO_BEER_DETAILS = 2;
    private Brewery mBrewery;
    private Profile mProfile;
    private BeersTried mBeersTried;
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    private DatabaseReference myRef;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_brewery);

        mAuth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        uid = user.getUid();



        Intent intent = getIntent();
        if (intent.hasExtra("BREWERY")) {
            Serializable brewery = intent.getSerializableExtra("BREWERY");
            if (brewery instanceof Brewery) {
                mBrewery = (Brewery) brewery;

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_selectedBrewery_container, SelectedBreweryFragment.newInstance((Brewery) brewery))
                        .commit();

            }
        } else if (intent.hasExtra("SELEC_BREWERY")) {
            Serializable brewery = intent.getSerializableExtra("SELEC_BREWERY");
            if (brewery instanceof Brewery) {
                mBrewery = (Brewery) brewery;

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_selectedBrewery_container, SelectedBreweryFragment.newInstance((Brewery) brewery))
                        .commit();
                Serializable beer = intent.getSerializableExtra("SELEC_BEER");
                if (beer instanceof BeersTried) {
                    Intent beerDetailsIntent = new Intent(this, BeerProfileActivity.class);
                    beerDetailsIntent.putExtra("BEER", beer);
                    startActivityForResult(beerDetailsIntent, TO_BEER_DETAILS);
                }

            }
        }



    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Return from AddBeerActivity
        if (requestCode == TO_ADD_BEER && resultCode == RESULT_OK) {
            if (data != null) {
                Serializable beer = data.getSerializableExtra("com.example.beerMe.NEWBEER");
                if (beer instanceof BeersTried) {
                    Log.d(TAG, "onActivityResult: " + ((BeersTried) beer).getName());
                    ArrayList<BeersTried> beersTrieds = new ArrayList<>();
                    if (mBrewery.getBeers() != null) {
                        beersTrieds = mBrewery.getBeers();
                    }

                    mBeersTried = (BeersTried) beer;
                    mBeersTried.setBreweryFrom(mBrewery.getName());
                    beersTrieds.add(mBeersTried);
                    mBrewery.setBeers(beersTrieds);
                    // Save to firebase
                    saveBreweryToFirebase();

                }
            }

        }
        // Return from BeerProfileActivity
        else if (requestCode == TO_BEER_DETAILS && resultCode == RESULT_OK) {
            if (data != null) {
                Serializable beer = data.getSerializableExtra("com.example.beerMe.BEERUPDATE");
                if (beer instanceof BeersTried) {
                    for (int i = 0; i < mBrewery.getBeers().size(); i++) {
                        if (mBrewery.getBeers().get(i).getId().equals(((BeersTried) beer).getId())) {
                            mBrewery.getBeers().set(i, (BeersTried) beer);
                            mBeersTried = (BeersTried) beer;
                            mBeersTried.setBreweryFrom(mBrewery.getName());
                            // Saving to Firebase
                            saveBreweryToFirebase();
                        }
                    }
                }
            }
        }

        // Load fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_selectedBrewery_container, SelectedBreweryFragment.newInstance(mBrewery))
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.selected_brewery_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        // Checking into brewery
        if (item.getItemId() == R.id.checkIn) {
            Date c = Calendar.getInstance().getTime();
            SimpleDateFormat df = new SimpleDateFormat("MMMM dd, yyyy", Locale.getDefault());
            String formattedDate = df.format(c);

            mBrewery.setLastVisited(formattedDate);
            int count = mBrewery.getTimesVisited();
            mBrewery.setTimesVisited(count + 1);

            // Saving to firebase
            saveBreweryToFirebase();

            // Load the fragment
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_selectedBrewery_container, SelectedBreweryFragment.newInstance(mBrewery))
                    .commit();
        }
        else if (item.getItemId() == R.id.review) {
            // Creating alert Dialog with one Button
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(SelectedBreweryActivity.this);

            // Setting Dialog Title
            alertDialog.setTitle("Rate " + mBrewery.getName());

            // Setting Dialog Message
            alertDialog.setMessage("Please rate between 1-5");
            final EditText input = new EditText(this);
            input.setInputType(InputType.TYPE_CLASS_NUMBER);
            alertDialog.setView(input);

            // Setting Icon to Dialog
            alertDialog.setIcon(R.drawable.review);

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("Submit",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int which) {
                            // Write your code here to execute after dialog
                            if (!input.getText().toString().trim().equals("")) {
                                int num = Integer.parseInt(input.getText().toString().trim());
                                if (num < 1 || num > 5) {
                                    Toast.makeText(SelectedBreweryActivity.this, "Please only enter ratings 1-5", Toast.LENGTH_SHORT)
                                            .show();
                                } else {
                                    // All checks pass
                                    mBrewery.setRating(num);
                                    saveBreweryToFirebase();
                                    getSupportFragmentManager().beginTransaction()
                                            .replace(R.id.fragment_selectedBrewery_container, SelectedBreweryFragment.newInstance(mBrewery))
                                            .commit();
                                }
                            } else {
                                Toast.makeText(SelectedBreweryActivity.this, "Please don't leave rating blank", Toast.LENGTH_SHORT)
                                        .show();
                        }
                    }
                    });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });

            alertDialog.show();
        }
        return true;
    }

    // Segue to AddBeerActivity
    @Override
    public void addBeer() {
        Intent addBeerIntent = new Intent(this, AddBeerActivity.class);
        startActivityForResult(addBeerIntent, TO_ADD_BEER);
    }

    // Segue to BeerProfileActivity
    @Override
    public void toBeerDetails(BeersTried beersTried) {
        Intent beerDetailsIntent = new Intent(this, BeerProfileActivity.class);
        beerDetailsIntent.putExtra("BEER", beersTried);
        startActivityForResult(beerDetailsIntent, TO_BEER_DETAILS);
    }

    // Return to BeerMapActivity
    @Override
    public void backToMap(Brewery brewery) {
        Intent returnToMapIntent = new Intent();
        returnToMapIntent.putExtra("BREWERY_RETURN", brewery);
        setResult(RESULT_OK, returnToMapIntent);
        finish();
    }



    private void saveBreweryToFirebase() {
        myRef = database.getReference("users/" +uid+"/Breweries Visited");
        Map<String, Object> userData = new HashMap<>();
        userData.put("name", mBrewery.getName());
        userData.put("rating", mBrewery.getRating());
        userData.put("timesVisited", mBrewery.getTimesVisited());
        userData.put("lastVisited", mBrewery.getLastVisited());
        userData.put("lat", mBrewery.getLat());
        userData.put("lon", mBrewery.getLon());
        myRef = myRef.child(mBrewery.getName());
        myRef.setValue(userData);

        if (mBrewery.getBeers() != null) {
        for (int i = 0; i < mBrewery.getBeers().size(); i ++) {
            myRef = database.getReference("users/" + uid + "/Breweries Visited/" + mBrewery.getName() + "/Favorite Beers");
            mBeersTried = mBrewery.getBeers().get(i);
            Map<String, Object> userData2 = new HashMap<>();
            userData2.put("id", mBeersTried.getId());
            userData2.put("name", mBeersTried.getName());
            userData2.put("rating", mBeersTried.getRating());
            userData2.put("cost", mBeersTried.getCost());
            userData2.put("abv", mBeersTried.getAbv());
            userData2.put("lastDrank", mBeersTried.getLastDrank());
            userData2.put("notes", mBeersTried.getNotes());
            userData2.put("amountDrank", mBeersTried.getAmountDrank());
            userData2.put("breweryFrom", mBeersTried.getBreweryFrom());
            myRef = myRef.child(mBeersTried.getName());
            myRef.setValue(userData2);
        }
        }


    }
}