package com.example.beerme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

public class SignUpActivity extends AppCompatActivity implements SignUpFragment.Listener {

    // Various Variables
    private static final String TAG = "SignUpActivity";
    private FirebaseAuth mAuth;
    private DatabaseReference usersRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        // Firebase
        mAuth = FirebaseAuth.getInstance();

        // Loading the fragment
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_signUp_container, SignUpFragment.newInstance())
                .commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: Operation has started");
    }

    @Override
    public void submitNewUser(String email, String password) {

        Log.d(TAG, "submitNewUser: " + email);
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, segue to BeerMapActivity with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            int success = 1;
                            Intent intent = new Intent(SignUpActivity.this, BeerMapActivity.class);
                            intent.putExtra("newUser1234", success);
                            startActivity(intent);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "Sign up failed... ", task.getException());
                            Toast.makeText(SignUpActivity.this, "Authentication failed. " +
                                            "Please check fields and try again.",
                                    Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}