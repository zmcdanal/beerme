package com.example.beerme;

import java.io.Serializable;
import java.util.ArrayList;

public class Profile implements Serializable {

    // Declarations
    private String creationDate;
    private int breweriesVisited;
    private String mostVisited;
    private double moneySpent;
    private ArrayList<BeersTried> favoriteBeers;

    public Profile() {

    }

    // Constructor
    public Profile(String creationDate, int breweriesVisited, String mostVisited, double moneySpent, ArrayList<BeersTried> favoriteBeers) {
        this.creationDate = creationDate;
        this.breweriesVisited = breweriesVisited;
        this.mostVisited = mostVisited;
        this.moneySpent = moneySpent;
        this.favoriteBeers = favoriteBeers;
    }

    // Getters
    public String getCreationDate() {
        return creationDate;
    }

    public int getBreweriesVisited() {
        return breweriesVisited;
    }

    public String getMostVisited() {
        return mostVisited;
    }

    public double getMoneySpent() {
        return moneySpent;
    }

    public ArrayList<BeersTried> getFavoriteBeers() {
        return favoriteBeers;
    }

    // Setters
    public void setBreweriesVisited(int breweriesVisited) {
        this.breweriesVisited = breweriesVisited;
    }

    public void setMostVisited(String mostVisited) {
        this.mostVisited = mostVisited;
    }

    public void setMoneySpent(double moneySpent) {
        this.moneySpent = moneySpent;
    }

    public void setFavoriteBeers(ArrayList<BeersTried> favoriteBeers) {
        this.favoriteBeers = favoriteBeers;
    }
}
