package com.example.beerme;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;

import java.io.Serializable;

public class UserProfileActivity extends AppCompatActivity implements UserProfileFragment.Listener {

    // Firebase declarations
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        // Firebase
        mAuth = FirebaseAuth.getInstance();

        // Obtain intent extra and load fragment
        Intent intent = getIntent();
        Serializable profile = intent.getSerializableExtra("PROFILE");
        if (profile instanceof Profile) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_userProfile_container, UserProfileFragment.newInstance((Profile) profile))
                    .commit();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.user_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.logout) {
            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            //Yes button clicked
                            mAuth.signOut();
                            Intent intent = new Intent(UserProfileActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            finish();
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Log Out").setMessage("Are you sure you wish to log out of this account?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();
        }

        return true;
    }

    // Return to BeerMapActivity to segue to SelectedBreweryActivity to then be routed to
    // BeerProfileActivity to view beer details
    @Override
    public void toBeerDetails(BeersTried beersTried) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("SELECTED_BEER", beersTried);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    // Return to BeerMapActivity
    @Override
    public void backToMap() {
        finish();
    }
}