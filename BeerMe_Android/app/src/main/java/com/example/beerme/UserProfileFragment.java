package com.example.beerme;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.ListFragment;

public class UserProfileFragment extends ListFragment {

    // Various Variables
    private static final String TAG = "UserProfileFragment";
    private Profile profile;
    private Listener mListener;

    public UserProfileFragment() {
    }

    public static UserProfileFragment newInstance(Profile profile) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, profile);
        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof UserProfileFragment.Listener) {
            mListener = (UserProfileFragment.Listener) context;
        }
    }

    // Interface
    public interface Listener {
        void toBeerDetails(BeersTried beersTried);
        void backToMap();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_profile, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null && getView() != null) {
            profile = (Profile) getArguments().getSerializable(TAG);

            if (profile != null) {

                // findViewByIds
                TextView moneySpent = getView().findViewById(R.id.moneySpentTV);
                TextView memberSince = getView().findViewById(R.id.memberSinceTV);
                TextView breweryCount = getView().findViewById(R.id.breweryCountTV);
                TextView mostVisited = getView().findViewById(R.id.mostVisitedTV);
                ImageButton backToMap = getView().findViewById(R.id.backToMapImgBtn);

                String moneySpentFinal = "$" + profile.getMoneySpent();

                // Setting text for page
                memberSince.setText(profile.getCreationDate());
                breweryCount.setText(String.valueOf(profile.getBreweriesVisited()));
                mostVisited.setText(profile.getMostVisited());
                moneySpent.setText(moneySpentFinal);

                // List Adapter is set
                setListAdapter(new BeersTriedListAdapter(profile.getFavoriteBeers(), getContext()));

                backToMap.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.backToMap();
                    }
                });
            }
        }
    }
    // To BeerProfileActivity
    @Override
    public void onListItemClick(@NonNull ListView l, @NonNull View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        mListener.toBeerDetails(profile.getFavoriteBeers().get(position));
    }
}
