package com.example.beerme;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import java.io.Serializable;

public class AddBeerActivity extends AppCompatActivity implements AddBeerFragment.Listener {

    // Intent names
    private static final String NEWBEER = "com.example.beerMe.NEWBEER";
    private static final String EDITBEER = "com.example.beerMe.EDITBEER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_beer);

        Intent intent = getIntent();
        // Edit beer intent is gathered
        if (intent.hasExtra("BEERTOCHANGE")) {
            Serializable beer = intent.getSerializableExtra("BEERTOCHANGE");
            if (beer instanceof BeersTried) {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_addBeer_container, AddBeerFragment.newInstance((BeersTried) beer))
                        .commit();
            }
        } else {
            // This is fired if user is here to add beer instead of edit.
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_addBeer_container, AddBeerFragment.newInstance(null))
                    .commit();
        }
    }

    // Return intent to submit new beer
    @Override
    public void submitNewBeer(BeersTried beersTried) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(NEWBEER, beersTried);
        setResult(RESULT_OK, returnIntent);
        finish();
    }


    // Return intent to submit edited beer
    @Override
    public void submitEditedBeer(BeersTried beersTried) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(EDITBEER, beersTried);
        setResult(RESULT_OK, returnIntent);
        finish();
    }


}