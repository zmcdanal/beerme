package com.example.beerme;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class BeersTriedListAdapter extends BaseAdapter {

    private static final String TAG = "BeersTriedListAdapter";

    // Declarations
    private final ArrayList<BeersTried> beersTrieds;
    private final Context mContext;

    public BeersTriedListAdapter(ArrayList<BeersTried> beersTrieds, Context mContext) {
        this.beersTrieds = beersTrieds;
        this.mContext = mContext;
    }

    @Override
    public int getCount() {
        // Return the number of items
        if (beersTrieds != null) {
            return beersTrieds.size();
        } else {
            Log.i(TAG, "getCount: There is no collection");
            return 0;
        }
    }

    @Override
    public BeersTried getItem(int position) {
        // Return an item within the collection at a specific position
        if (beersTrieds != null && position >= 0 && position < beersTrieds.size()) {
            return beersTrieds.get(position);
        } else {
            Log.i(TAG, "getItem: There was a problem retrieving an item");
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        // Return a unique ID for a specific postion
        if (beersTrieds != null && position >= 0 && position < beersTrieds.size()) {
            return position;
        } else {
            Log.i(TAG, "getItemId: There was a problem assigning an ID");
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Return a child view for specific position
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.custom_beer_list_item,
                    parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BeersTried beersTried = getItem(position);

        holder.titleTV.setText(beersTried.getName());

        if (beersTried.getRating() >= 1) {
            holder.star1.setImageResource(R.drawable.star);
        } else {
            holder.star1.setImageResource(R.drawable.emptystar);
        }
        if (beersTried.getRating() >= 2) {
            holder.star2.setImageResource(R.drawable.star);
        } else {
            holder.star2.setImageResource(R.drawable.emptystar);
        }
        if (beersTried.getRating() >= 3) {
            holder.star3.setImageResource(R.drawable.star);
        } else {
            holder.star3.setImageResource(R.drawable.emptystar);
        }
        if (beersTried.getRating() >= 4) {
            holder.star4.setImageResource(R.drawable.star);
        } else {
            holder.star4.setImageResource(R.drawable.emptystar);
        }
        if (beersTried.getRating() == 5) {
            holder.star5.setImageResource(R.drawable.star);
        } else {
            holder.star5.setImageResource(R.drawable.emptystar);
        }

        return convertView;
    }

    // View Holder
    static class ViewHolder {

        final TextView titleTV;
        final ImageView star1;
        final ImageView star2;
        final ImageView star3;
        final ImageView star4;
        final ImageView star5;

        ViewHolder(View v) {
            titleTV = v.findViewById(R.id.titleTV);
            star1 = v.findViewById(R.id.starList1);
            star2 = v.findViewById(R.id.starList2);
            star3 = v.findViewById(R.id.starList3);
            star4 = v.findViewById(R.id.starList4);
            star5 = v.findViewById(R.id.starList5);
        }
    }
}
