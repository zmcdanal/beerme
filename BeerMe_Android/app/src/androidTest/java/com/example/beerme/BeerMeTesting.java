package com.example.beerme;



import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.intent.rule.IntentsTestRule;
import androidx.test.uiautomator.UiDevice;
import androidx.test.uiautomator.UiObject;
import androidx.test.uiautomator.UiScrollable;
import androidx.test.uiautomator.UiSelector;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class BeerMeTesting {

    @Rule
    public IntentsTestRule<LoginActivity> loginActivityAutomatorTestRule =
            new IntentsTestRule<>(
                    LoginActivity.class,
                    true,
                    false);

    @Test
    public void testCreateAccount() throws Exception {
        loginActivityAutomatorTestRule.launchActivity(null);

        UiDevice device = UiDevice.getInstance(
                InstrumentationRegistry.getInstrumentation());

        device.pressHome();

        UiScrollable scroll = new UiScrollable(new UiSelector().scrollable(true));
        scroll.scrollForward();

        UiObject appDrawerButton = device
                .findObject(new UiSelector().text("BeerMe"));

        appDrawerButton.clickAndWaitForNewWindow();

        UiObject creationBtn = device.findObject(new UiSelector().resourceId("com.example.beerme:id/createAccountTVBtn"));
        creationBtn.clickAndWaitForNewWindow();

        UiObject emailET = device.findObject(new UiSelector().resourceId("com.example.beerme:id/emailET"));
        emailET.setText("test@hotmail.com");
        UiObject passwordET = device.findObject(new UiSelector().resourceId("com.example.beerme:id/passwordET"));
        passwordET.setText("temp12345");

        UiObject checkBX = device.findObject(new UiSelector().resourceId("com.example.beerme:id/termsChkBx"));
        checkBX.click();

        UiObject submitBtn = device.findObject(new UiSelector().description("signup"));
        submitBtn.clickAndWaitForNewWindow();
    }

    @Test
    public void testSignIn() throws Exception {

        loginActivityAutomatorTestRule.launchActivity(null);

        UiDevice device = UiDevice.getInstance(
                InstrumentationRegistry.getInstrumentation());

        device.pressHome();

        UiScrollable scroll = new UiScrollable(new UiSelector().scrollable(true));
        scroll.scrollForward();

        UiObject appDrawerButton = device
                .findObject(new UiSelector().text("BeerMe"));

        appDrawerButton.clickAndWaitForNewWindow();

        UiObject emailET = device.findObject(new UiSelector().resourceId("com.example.beerme:id/emailET"));
        emailET.setText("test@hotmail.com");
        UiObject passwordET = device.findObject(new UiSelector().resourceId("com.example.beerme:id/passwordET"));
        passwordET.setText("temp12345");

        UiObject submitBtn = device.findObject(new UiSelector().description("submit"));
        submitBtn.clickAndWaitForNewWindow();
    }
}