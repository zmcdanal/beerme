# BeerMe #

All progress updates will be listed below.

## Requirments: ##
* Android Phone APK 21+
* Free Local Brewery API shut down so temporary json file with set locations was added
* Internet Access a HUGE must - Map/Database will struggle to load if low bars
* Portrait Mode Only
* Please allow map time to refresh if less than full wifi bars
* Sign up email and password used must follow typical google email/password guidelines
* Firebase will round your "Money Spent on Beer" amount to the nearest whole number
* If sign in/account creation never transitions to google map, please allow time for database to return user info.

### August 2, 2020 ###
* Various bugs fixed
* Unused code removed

### July 29, 2020 ###
* Finished BeerMe
* Code-Comments added

### July 26, 2020 ###
* Api Server went down preventing progress
* App isnt functional without new data coming in

### July 25, 2020 ###

* User Profile was added
* All other pages finished except for minor fixes and data saving
* Database saving beginning to take shape

### July 18, 2020 ###

* Google Map page finished
* Brewery page almost finished
* Individual Beer page in the works 

### July 17, 2020 ###

* Login page UI created with buttons functioning correctly
* Sign Up page UI created 
* Google Maps API obtained






